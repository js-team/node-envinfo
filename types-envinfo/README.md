# Installation
> `npm install --save @types/envinfo`

# Summary
This package contains type definitions for envinfo (https://github.com/tabrindle/envinfo#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/envinfo.

### Additional Details
 * Last updated: Mon, 06 Nov 2023 22:41:05 GMT
 * Dependencies: none

# Credits
These definitions were written by [nashaofu](https://github.com/nashaofu).
